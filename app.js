
//[SECTION] PACKAGES AND DEPENDENCIES  
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	
	// const orderRoutes = require('./routes/order');
	// const productRoutes = require('./routes/product');
	 const userRoutes = require('./routes/users');

//[SECTION]Server Setup
	
	const app = express();
	dotenv.config(); 
	app.use(express.json());
	const secret = process.env.CONNECTION_STRING;
	const port = process.env.PORT;
	
//[SECTION] Application Routes
	
	// app.use('/order', orderRoutes);
	 app.use('/users', userRoutes);
	// app.use('/product', productRoutes);
	
	

//[SECTION] Database Connection
	mongoose.connect(secret);
	let connectStatus = mongoose.connection;
	connectStatus.on('open',()=> console.log(`Database is Connected`));


//[SECTION] Gateway Response
app.get('/', (req,res)=>{
	res.send(`Welcome to Cj Quimpo's Online Grocery`)
});
app.listen(port,()=> console.log(`Server is running on port ${port}`));

