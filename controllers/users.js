//[SECTION] Dependencies and Modules
  const User = require('../models/User');
  const bcrypt = require("bcrypt");
  const dotenv = require("dotenv"); 
  
//[SECTION] Environment Setup
  dotenv.config();
  const salt = Number(process.env.SALT); 

//[SECTION] Functionalities [CREATE]
  module.exports.registerUser = (data) => {
  	let email = data.email;
  	let passW = data.password;
  	let newUser = new User({
  		email: email,
  		password: bcrypt.hashSync(passW, salt)
  	}); 
  	return newUser.save().then((user, rejected) => {
  		if (user) {
  			return user; 
  		} else {
  			return 'Failed to Register a new account'; 
  		}; 
  	});
  };

  		
 
//[SECTION] Functionalities [RETRIEVE]
//[SECTION] Functionalities [UPDATE]
//[SECTION] Functionalities [DELETE]
