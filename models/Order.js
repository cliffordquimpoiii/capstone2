//[SECTION] Dependencies and Modules
	const mongoose = require("mongoose");
//[SECTION] Schema/Document Blueprint
	const orderSchema = new mongoose.Schema({
		totalAmount:{
			type:Number,
			default: Number
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		},
		productList:[
			{
					userId: {
					type: String,
					required: [true, "User's ID is Required"]
				},
				product:{
					type: Array,
					required: [true, "User's ID is Required"]
				
				}

			}
		]


	});

	//[SECTION] MOdel
	const Order = mongoose.model('Order', orderSchema);
	module.exports = Order;
