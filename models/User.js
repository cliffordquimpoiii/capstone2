//[SECTION] Deoendecies and Modules
	const mongoose = require('mongoose');
//[SECTION] Blueprint Schema
	const userSchema = new mongoose.Schema({
		email:{
			type:String,
			required:[true, 'Email is required']
		},
		password:{
			type:String,
			required:[true, 'Password is required']
		},
		isAdmin:{
			type:Boolean,
			default: false
		}
	});

//[SECTION] MOdel
	const User = mongoose.model('User', userSchema);
	module.exports = User;

